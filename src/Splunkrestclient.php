<?php

namespace Uncgits\Splunkrestclient;

use GuzzleHttp\Client;

class Splunkrestclient {

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    private $apiHost; // host and authentication method only, no protocol
    private $port;

    private $token;

    private $proxyHost;
    private $proxyPort;

    private $useProxy = false;

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * @param mixed $apiHost
     */
    public function setApiHost($apiHost) {
        $this->apiHost = $apiHost;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token) {
        $this->token = $token;
    }

    /**
     * @param mixed $proxyHost
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * @param mixed $proxyPort
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * @param boolean $useProxy
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }



    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to Splunk APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * apiCall() - performs a call to the Splunk API
     *
     * @param string $endpoint - API endpoint (per Splunk API docs)
     * @param string $method - Request method (e.g. GET, POST, HEAD, etc.)
     * @param string $params - parameters for the API call (optional)
     * @param string $download - boolean, set to true if the result is a download
     *
     * @return array
     */

    protected function apiCall($endpoint, $method, $params = []) {

        $requiredProperties = ['apiHost', 'token'];
        foreach ($requiredProperties as $property) {
            if ($this->$property === null) {
                throw new \Exception("Error: required property '$property' has not been set in API object.");
            }
        }

        // assemble the request target and endpoint
        $endpoint = $this->apiHost . ':' . $this->port . $endpoint;

        // instantiate Guzzle client

        $client = new Client();

        // set up basic options
        $requestOptions = [
            'http_errors' => false, // don't throw exceptions on HTTP errors
            'json' => $params
        ];

        // params / body

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        } else {
            $requestOptions['proxy'] = '';
        }

        $requestOptions['headers'] = [
            //'Content-Type' => 'application/json',
            'Authorization' => 'Splunk ' . $this->token,
        ];

        // perform the call
        $response = $client->$method($endpoint, $requestOptions);

        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = $body->getContents();

        $headers = $response->getHeaders();

        $bodyResults = json_decode($bodyContents);

        // return raw data about the request so that it can be parsed by the calling function
        return array(
            'response' => array(
                'endpoint'          => $endpoint,
                'method'            => $method,
                'httpCode'          => $code,
                'httpReason'        => $reason,
                'headers'           => $headers,
            ),
            'body'     => $bodyResults
        );
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    | Methods that perform common operations between other outward-facing
    | methods (e.g. getting a list of users)
    |
    */

    /**
     * checkOptions() - helper function to check provided options against a list of accepted options for an endpoint
     *
     * @param $acceptedOptions
     * @param $options
     *
     * @throws \Exception
     */
    public function checkOptions($acceptedOptions, $options) {

        $result = array_diff_key($this->makeFlat($options), $this->makeFlat($acceptedOptions));

        if(!empty($result)) {
            throw new \Exception("Error: One or more keys not valid for this endpoint: " . implode(', ', array_keys($result)));
        };

    }

    /*
     * 2 functions to flatten multidimensional arrays.
     * Used for flattening the key values of the parameters/options passed to the APIs,
     * which are then compared to determine if what's passed in is allowed.
     *
     * Usage for this API Client:
     * array_diff_key(makeFlat($options), makeFlat($allowedOptions)));
     * If empty, then all the keys in $options are allowed.
     * If not empty, then the resulting keys are not permitted.
     */

    public function flatten(array &$out, $key, array $in, $separator = ' => '){
        foreach($in as $k=>$v){
            if(is_array($v)){
                $this->flatten($out, $key . $k . $separator, $v);
            }else{
                $out[$key . $k] = $v;
            }
        }
    }

    public function makeFlat(array $in){
        $out = array();
        $this->flatten($out, '', $in);
        return $out;
    }

    /*
     * Parse HTTP Link headers into a structured format
     * Based on https://github.com/indieweb/link-rel-parser-php
     *
     */
    public function parse_http_rels($h) {

        $h = explode(",", $h);

        $replaceValuesArray = array(
            '<',
            '>; ',
            'rel="current"',
            'rel="first"',
            'rel="prev"',
            'rel="last"',
            'rel="next"',
            'https://' . $this->apiHost,
        );

        $returnArray = [];

        foreach($h as $header) {

            if(strstr($header, 'rel="current"')) {
                $returnArray['current'] = str_replace($replaceValuesArray, '', $header);
            } elseif(strstr($header, 'rel="first"')) {
                $returnArray['first'] = str_replace($replaceValuesArray, '', $header);
            } elseif(strstr($header, 'rel="prev"')) {
                $returnArray['prev'] = str_replace($replaceValuesArray, '', $header);
            } elseif(strstr($header, 'rel="next"')) {
                $returnArray['next'] = str_replace($replaceValuesArray, '', $header);
            } elseif(strstr($header, 'rel="last"')) {
                $returnArray['last'] = str_replace($replaceValuesArray, '', $header);
            }

        }

        //dd($h, $returnArray);


        return $returnArray;
    }


    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    |
    | API methods - note that this will likely expand as the API is fleshed out
    |
    */

    public function sendData($options = []) {

        return $this->apiCall('/services/collector', 'post', $options);
    }

}