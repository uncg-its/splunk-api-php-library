# Splunk API PHP Library

Contact: its-laravel-devs-l@uncg.edu

# Introduction

This package is a PHP library used to interact with the Splunk HTTP Event Collector API.

> This is a **work in progress**. Not recommend for production apps just yet.

## Scope

This package is built not (yet) as a comprehensive interface with the Splunk API - it is not written to perform every conceivable API call to Splunk. Specifically, this package was built to suit the needs of UNC Greensboro's Splunk HTTP Event Collector integration, and performs only the functions necessary for day-to-day operations of this configuration at UNCG.

Please fork and add methods to this as you see fit / as your needs dictate.

# Installation

1. `composer require 'uncgits/splunk-rest-client'`
2. Use one of the API calls built into the `Uncgits\Splunkrestclient\Splunkrestclient` class, or extend it and add your own. The standard API call is built in there. You'll have to find a way to call the setter methods to set the variables required to make the call - specific to your environment (e.g. urls, credentials, etc.)

# Version History

## 0.2.0

- Open Source BSD license

## 0.1.1

- update guzzle dependency for more flexibility

## 0.1

- official versioning
- psr-4
- basic functionality, needs to be fleshed out if we ever need more robust performance
